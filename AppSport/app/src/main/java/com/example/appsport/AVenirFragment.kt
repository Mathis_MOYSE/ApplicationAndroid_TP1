package com.example.appsport

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.appsport.databinding.AVenirBinding
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Retrofit
import retrofit2.Response
import retrofit2.converter.gson.GsonConverterFactory

class AVenirFragment : Fragment(R.layout.a_venir) {

    val id : String = "AVenirFragment";
    override fun onCreateView(
    inflater: LayoutInflater,
    container: ViewGroup?,
    savedInstanceState: Bundle?
    ): View? {
        val retrofitBuilder = Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create())
            .baseUrl(URL)
            .build()
            .create(InterfaceAPI::class.java)

        val PartieAVenir = retrofitBuilder.getPartieAVenir()
        var listPartieAVenir = listOf<PartieItem>()
        var binding:AVenirBinding = AVenirBinding.inflate(layoutInflater)

        var view:View = binding.root

        viewModel = ViewModelProvider(this).get(ListMatch::class.java)

        val recyclerView = view.findViewById<RecyclerView>(R.id.MatchRecyclerViewASUIVRE)
        recyclerView.layoutManager = LinearLayoutManager(context)

        val no_match = view.findViewById<TextView>(R.id.no_match)

        PartieAVenir.enqueue(object : Callback<List<PartieItem>?> {
            override fun onResponse(
                call: Call<List<PartieItem>?>,
                response: Response<List<PartieItem>?>
            ) {
                val reponse = response.body()
                if(reponse != null){
                    listPartieAVenir = reponse
                    if (listPartieAVenir.isEmpty()){
                        no_match.setTextColor(Color.WHITE)
                        no_match.text = "AUCUN MATCH A VENIR"
                    }
                    else{
                        no_match.text = ""
                        viewModel.setList(listPartieAVenir)
                        viewModel.matchList.observe(viewLifecycleOwner, Observer{
                            recyclerView.adapter = MatchRecyclerViewAdaptater(it){
                                    selectedItem: PartieItem -> listItemClicked(selectedItem)
                            }
                        })
                    }
                }
                else{
                    no_match.setTextColor(Color.WHITE)
                    no_match.text = "AUCUN MATCH A VENIR"
                }
            }

            override fun onFailure(call: Call<List<PartieItem>?>, t: Throwable) {
                no_match.setTextColor(Color.RED)
                no_match.text = "ECHEC DE RECUPERATION DES DONNEES"
                Log.d("Data Not Found", "fail to get data, erreur" + t)
            }
        })

        return view;
    }

    private fun listItemClicked(match: PartieItem) {
        val intent = Intent(context, DetailMatchActivity::class.java);
        intent.putExtra("partie", match)
        context?.startActivity(intent);
    }
}