package com.example.appsport

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.Telephony.Mms.Part
import android.util.Log
import android.widget.TextView
import android.widget.Toast
import androidx.activity.ComponentActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.appsport.databinding.ActivityDetailMatchBinding
import com.example.appsport.databinding.ActivityMainBinding
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.text.SimpleDateFormat
import java.util.Date

class DetailMatchActivity : ComponentActivity() {

    private lateinit var binding: ActivityDetailMatchBinding


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityDetailMatchBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val extraData = (getIntent().getExtras()?.getSerializable("partie"));

        val recyclerView = findViewById<RecyclerView>(R.id.RecyclerViewDetail)
        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.adapter = DetailMatchRecyclerViewAdaptater(extraData as PartieItem) {
                selectedItem: PartieItem -> listItemClicked(selectedItem)
        }

        binding.ButtonRefreshDetailA.setOnClickListener{

            val retrofitBuilder = Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(URL)
                .build()
                .create(InterfaceAPI::class.java)

            val UpdatePartie = retrofitBuilder.getPartieAvecID(extraData.id_partie)

            UpdatePartie.enqueue(object : Callback<PartieItem> {
                override fun onResponse(call: Call<PartieItem>, response: Response<PartieItem>) {

                    val reponse = response.body()
                    if (reponse != null) {
                        Log.d("GET PARTIE ID WITH ID OK", "PARTIE ID = " + reponse.id_partie.toString())
                        val recyclerView = findViewById<RecyclerView>(R.id.RecyclerViewDetail)
                        recyclerView.layoutManager = LinearLayoutManager(this@DetailMatchActivity)
                        recyclerView.adapter = DetailMatchRecyclerViewAdaptater(reponse as PartieItem) {
                                selectedItem: PartieItem -> listItemClicked(selectedItem)
                        }

                    }
                }

                override fun onFailure(call: Call<PartieItem>, t: Throwable) {
                    Log.d("GET PARTIE ID WITH ID FAILED", t.toString())
                }
            })

        }

    }

    private fun listItemClicked(selectedItem: PartieItem) {

    }
}