package com.example.appsport

import android.graphics.Color
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


class DetailMatchRecyclerViewAdaptater(
    private val partieContent : PartieItem,
    private val clickListener: (PartieItem)->Unit
    ):RecyclerView.Adapter<DetailMatchViewHolder>(){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DetailMatchViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val listItemDetail = layoutInflater.inflate(R.layout.list_item_detail, parent, false)
        return DetailMatchViewHolder(listItemDetail)
    }

    override fun onBindViewHolder(holder: DetailMatchViewHolder, position: Int) {

        holder.bind(partieContent, clickListener)
        holder.bouton_parier.setOnClickListener() {
            Log.d("BOUTON", "BOUTON PARIER PRESSÉ")
            //FORMULAIRE DANS UNE POP-UP
            // ID USER, JOUEUR MISE, MONTANT
            val montant_paris = holder.montant_paris.text.toString()
            val pseudo_parieur = holder.pseudo_parieur.text.toString()
            if (montant_paris != "" && montant_paris.isNotEmpty() && pseudo_parieur != "") {

                val joueur_choisi = holder.input_spinner_joueurs.selectedItem.toString();

                val retrofitBuilder = Retrofit.Builder()
                    .addConverterFactory(GsonConverterFactory.create())
                    .baseUrl(URL)
                    .build()
                    .create(InterfaceAPI::class.java)

                val Parier = retrofitBuilder.parierPartie(partieContent.id_partie, pseudo_parieur, joueur_choisi, montant_paris.toDouble())

                Parier.enqueue(object : Callback<MessageServeur> {
                    override fun onResponse(call: Call<MessageServeur>, response: Response<MessageServeur>) {

                        val reponse = response.body()

                        if (reponse != null) {
                            Log.d("REPONSE PARIS ", reponse.message)
                            if (reponse.message == "Paris enregistré") {
                                holder.liste_parieurs.add(pseudo_parieur)
                            }
                            holder.toast(reponse.message)
                        }
                    }

                    override fun onFailure(call: Call<MessageServeur>, t: Throwable) {
                        Log.d("FAILURE PARIS : ", "CA MARCHE PAS")
                    }
                })

            }

        }
    }

    override fun getItemCount(): Int {
        return 1;
    }
}