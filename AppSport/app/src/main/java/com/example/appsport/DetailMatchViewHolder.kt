package com.example.appsport

import android.graphics.Color
import android.view.View
import android.widget.ArrayAdapter
import android.widget.Button
import android.widget.Spinner
import android.widget.TextView
import android.widget.Toast
import androidx.compose.ui.text.toUpperCase
import androidx.recyclerview.widget.RecyclerView
import kotlinx.coroutines.delay
import java.util.Locale

class DetailMatchViewHolder(val view: View) : RecyclerView.ViewHolder(view) {

    val liste_parieurs = ArrayList<String>()
    val liste_paris_gagnants = ArrayList<Paris>()
    val bouton_parier = view.findViewById<Button>(R.id.Bouton_Detail_PARIER)
    val pseudo_parieur = view.findViewById<TextView>(R.id.TextView_Detail_Edit_ID)
    val montant_paris = view.findViewById<TextView>(R.id.TextView_Detail_Edit_Montant)
    val input_spinner_joueurs = view.findViewById<Spinner>(R.id.TextView_Detail_Edit_Spinner_Joueurs)
    val joueur_1_nom = view.findViewById<TextView>(R.id.TextView_Detail_J1)
    val joueur_2_nom = view.findViewById<TextView>(R.id.TextView_Detail_J2)
    val tournoi = view.findViewById<TextView>(R.id.TextView_Detail_Tournoi)
    val terrain = view.findViewById<TextView>(R.id.TextView_Detail_Terrain)
    val duree = view.findViewById<TextView>(R.id.TextView_Detail_Duree)
    val joueur_1_score = view.findViewById<TextView>(R.id.TextView_Detail_SCORE_J1)
    val joueur_2_score = view.findViewById<TextView>(R.id.TextView_Detail_SCORE_J2)
    val joueur_1_jeu_actuel = view.findViewById<TextView>(R.id.TextView_Detail_JEU_J1)
    val joueur_2_jeu_actuel = view.findViewById<TextView>(R.id.TextView_Detail_JEU_J2)
    val match_en_cours = view.findViewById<TextView>(R.id.TextView_Detail_Match_En_Cours)
    val joueur_1_pays = view.findViewById<TextView>(R.id.TextView_Detail_Pays_J1)
    val joueur_2_pays = view.findViewById<TextView>(R.id.TextView_Detail_Pays_J2)
    val joueur_1_rang = view.findViewById<TextView>(R.id.TextView_Detail_Rang_J1)
    val joueur_2_rang = view.findViewById<TextView>(R.id.TextView_Detail_Rang_J2)
    val joueur_1_contestations_restantes = view.findViewById<TextView>(R.id.TextView_Detail_C_content_J1)
    val joueur_2_contestations_restantes = view.findViewById<TextView>(R.id.TextView_Detail_C_content_J2)
    val joueur_1_nom_prenom = view.findViewById<TextView>(R.id.TextView_Detail_J1_NOMPRENOM)
    val joueur_2_nom_prenom = view.findViewById<TextView>(R.id.TextView_Detail_J2_NOMPRENOM)

    fun bind(partieItem : PartieItem, clickListener: (PartieItem) -> Unit) {

        val noms_joueurs = arrayOf(partieItem.joueur1.nom.uppercase(), partieItem.joueur2.nom.uppercase())

        if (input_spinner_joueurs != null) {
            val adapter = ArrayAdapter(view.context, android.R.layout.simple_spinner_item, noms_joueurs)
            input_spinner_joueurs.adapter = adapter
        }

        joueur_1_nom.text = partieItem.joueur1.nom.uppercase()
        joueur_2_nom.text = partieItem.joueur2.nom.uppercase()
        joueur_1_pays.text = partieItem.joueur1.pays
        joueur_2_pays.text = partieItem.joueur2.pays
        joueur_1_rang.text = "Rang (ATP) : "+partieItem.joueur1.rang.toString()
        joueur_2_rang.text = "Rang (ATP) : "+partieItem.joueur2.rang.toString()

        tournoi.text = partieItem.tournoi.uppercase()
        terrain.text = "TERRAIN : "+partieItem.terrain
        duree.text = partieItem.temps_partie.toString() + " mn"

        if (partieItem.pointage.fini != true && partieItem.temps_partie == 0) {
            match_en_cours.text = "MATCH A VENIR..."
            match_en_cours.setTextColor(Color.YELLOW)
        }
        else if (partieItem.pointage.fini != true) {
            match_en_cours.text = "MATCH EN COURS..."
            match_en_cours.setTextColor(Color.GREEN)
        }
        else {
            match_en_cours.text = "MATCH TERMINÉ..."
            match_en_cours.setTextColor(Color.RED)

            //On recherche des gagnants éventuels !!!
            for (element : String in liste_parieurs) {
                var paris = partieItem.liste_gagnants.find { paris -> paris.id_parieur == liste_parieurs[1] }
                if (paris != null) {
                    liste_paris_gagnants.add(paris)
                }
            }

            for (gagnant : Paris in liste_paris_gagnants) {

                toast( "GAGNANT ID : " + gagnant.id_parieur.toString() +
                        " joueur misé " + gagnant.joueur_mise +
                        " gain : " + gagnant.gain + "€" )
                }
            }

        joueur_1_nom_prenom.text = partieItem.joueur1.prenom.uppercase() + "\n" + partieItem.joueur1.nom.uppercase()
        joueur_2_nom_prenom.text = partieItem.joueur2.prenom.uppercase() + "\n" + partieItem.joueur2.nom.uppercase()

        var joueur_1_score_string = "   "
        var joueur_2_score_string = "   "

        for (score in partieItem.pointage.jeu ) {
            joueur_1_score_string += score[0].toString() + "   "
            joueur_2_score_string += score[1].toString() + "   "
        }

        joueur_1_score.text = joueur_1_score_string
        joueur_2_score.text = joueur_2_score_string

        var jeu_actual_j1_a_afficher = 0
        var jeu_actual_j2_a_afficher = 0

        if (partieItem.pointage.echange[0] == 1) {
            jeu_actual_j1_a_afficher = 15;
        }
        if (partieItem.pointage.echange[0] == 2) {
            jeu_actual_j1_a_afficher = 30;
        }
        if (partieItem.pointage.echange[0] == 3) {
            jeu_actual_j1_a_afficher = 40;
        }

        if (partieItem.pointage.echange[1] == 1) {
            jeu_actual_j2_a_afficher = 15;
        }
        if (partieItem.pointage.echange[1] == 2) {
            jeu_actual_j2_a_afficher = 30;
        }
        if (partieItem.pointage.echange[1] == 3) {
            jeu_actual_j2_a_afficher = 40;
        }

        joueur_1_jeu_actuel.text = " " + jeu_actual_j1_a_afficher.toString() + " "
        joueur_2_jeu_actuel.text = " " + jeu_actual_j2_a_afficher.toString() + " "

        joueur_1_contestations_restantes.text = partieItem.contestation[0].toString() + " restante(s)"
        joueur_2_contestations_restantes.text = partieItem.contestation[1].toString() + " restante(s)"

        view.setOnClickListener{
            clickListener(partieItem)
        }
    }

    fun toast(message : String) {
        Toast.makeText(
            view.context,
            message,
            Toast.LENGTH_LONG
        ).show()
    }

}