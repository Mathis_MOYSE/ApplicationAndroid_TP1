package com.example.appsport

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path

interface InterfaceAPI {

    @GET("parties")
    fun getPartie() : Call<List<PartieItem>>

    @GET("parties/en_cours")
    fun getPartieEnCours() : Call<List<PartieItem>>

    @GET("parties/terminees")
    fun getPartieTerminees() : Call<List<PartieItem>>

    @GET("parties/a_venir")
    fun getPartieAVenir() : Call<List<PartieItem>>

    @GET("parties/{id}")
    fun getPartieAvecID(@Path("id") id_partie: Int): Call<PartieItem>

    @GET("parties/{id}/parier/{id_parieur}/{joueur}/{montant_mise}")
    fun parierPartie(@Path("id") id_partie: Int,
                     @Path("id_parieur") id_parieur: String,
                     @Path("joueur") joueur: String,
                     @Path("montant_mise") montant_mise: Double): Call<MessageServeur>
}