package com.example.appsport

import java.io.Serializable

data class Joueur (
    val age: Int,
    val nom: String,
    val pays: String,
    val prenom: String,
    val rang: Int
) : Serializable