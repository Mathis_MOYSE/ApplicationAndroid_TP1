package com.example.appsport

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class ListMatch : ViewModel(){
    var matchList = MutableLiveData<List<PartieItem>>()

    fun setList(list : List<PartieItem>){
        matchList = MutableLiveData<List<PartieItem>>(list)
    }
}