package com.example.appsport

import android.content.Intent
import android.content.res.Configuration
import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.appsport.databinding.ActivityMainBinding
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.NonCancellable
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

const val URL = "http://10.0.2.2:8080/"
lateinit var viewModel:ListMatch


class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    private lateinit var fragment: EnCoursFragment

    override fun onResume() {
        super.onResume()
        fragment = EnCoursFragment()
        replacerFragment(fragment)

        binding = ActivityMainBinding.inflate(layoutInflater)
        binding.fragment2Btn?.performClick()

        actualiserDonneesMainAuto(60000L)
    }

    private fun actualiserDonneesMainAuto(timeInterval: Long): Job {
        return CoroutineScope(Dispatchers.Default).launch {
            while (NonCancellable.isActive) {
                Log.d("TEST COROUTINE ACTUALISATION AUTO", "COROUTINE !!!!!!")
                replacerFragment(fragment)
                binding = ActivityMainBinding.inflate(layoutInflater)

                if(fragment.id == "EnCoursFragment") {
                    var fragment = EnCoursFragment()
                    binding.fragment2Btn?.performClick()
                }

                else if(fragment.id == "AVenirFragment") {
                    var fragment = AVenirFragment()
                    binding.fragment3Btn?.performClick()
                }

                else if(fragment.id == "TermineFragment") {
                    var fragment = TermineFragment()
                    binding.fragment1Btn?.performClick()
                }

                delay(timeInterval)
            }
        }
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        var config = resources.configuration

        if(config.orientation == Configuration.ORIENTATION_PORTRAIT){
            fragment = EnCoursFragment()
            replacerFragment(fragment)

            binding.fragment1Btn?.setOnClickListener{
                var fragment = TermineFragment()
                replacerFragment(fragment)
            }


            binding.fragment2Btn?.setOnClickListener{
                var fragment = EnCoursFragment()
                replacerFragment(fragment)
            }

            binding.fragment3Btn?.setOnClickListener{
                var fragment = AVenirFragment()
                replacerFragment(fragment)
            }

            binding.ButtonRefreshMainA.setOnClickListener{
                Log.d("BUTTON REFRESH PRESSED", "OK")
                replacerFragment(fragment)
            }
        }
        else if(config.orientation == Configuration.ORIENTATION_LANDSCAPE){
            val retrofitBuilder = Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(URL)
                .build()
                .create(InterfaceAPI::class.java)

            val PartieAVenir = retrofitBuilder.getPartieAVenir()
            val PartieEnCours = retrofitBuilder.getPartieEnCours()
            val PartieTerminees = retrofitBuilder.getPartieTerminees()

            var listPartieAVenir = listOf<PartieItem>()
            var listPartieEnCours = listOf<PartieItem>()
            var listPartieTermine = listOf<PartieItem>()

            var view:View = binding.root
            viewModel = ViewModelProvider(this).get(ListMatch::class.java)

            val recyclerViewAS = view.findViewById<RecyclerView>(R.id.MatchRecyclerViewASUIVRE)
            val recyclerViewEC = view.findViewById<RecyclerView>(R.id.MatchRecyclerViewENCOURS)
            val recyclerViewT = view.findViewById<RecyclerView>(R.id.MatchRecyclerViewTERMINES)

            recyclerViewAS.layoutManager = LinearLayoutManager(this)
            recyclerViewEC.layoutManager = LinearLayoutManager(this)
            recyclerViewT.layoutManager = LinearLayoutManager(this)

            val no_match_T = view.findViewById<TextView>(R.id.no_match_T)
            val no_match_EC = view.findViewById<TextView>(R.id.no_match_EC)
            val no_match_AV = view.findViewById<TextView>(R.id.no_match_AV)

            PartieAVenir.enqueue(object : Callback<List<PartieItem>?> {
                override fun onResponse(
                    call: Call<List<PartieItem>?>,
                    response: Response<List<PartieItem>?>
                ) {
                    val reponse = response.body()
                    if(reponse != null){
                        listPartieAVenir = reponse
                        if (listPartieAVenir.isEmpty()){
                            no_match_AV.text = "AUCUN MATCH A VENIR"
                        }
                        else{
                            no_match_AV.text = ""
                            viewModel.setList(listPartieAVenir)
                            viewModel.matchList.observe(this@MainActivity, Observer{
                                recyclerViewAS.adapter = MatchRecyclerViewAdaptater(it){
                                        selectedItem: PartieItem -> listItemClicked(selectedItem)
                                }
                            })
                        }
                    }
                    else{
                        no_match_T.setTextColor(Color.WHITE)
                        no_match_AV.text = "AUCUN MATCH A VENIR"
                    }
                }

                override fun onFailure(call: Call<List<PartieItem>?>, t: Throwable) {
                    no_match_T.setTextColor(Color.RED)
                    no_match_AV.text = "ECHEC DE RECUPERATION DES DONNEES"
                    Log.d("Data Not Found PARTIE A VENIR", "fail to get data, erreur" + t)
                }
            })
            PartieEnCours.enqueue(object : Callback<List<PartieItem>?> {
                override fun onResponse(
                    call: Call<List<PartieItem>?>,
                    response: Response<List<PartieItem>?>
                ) {
                    val reponse = response.body()
                    if(reponse != null){
                        listPartieEnCours = reponse
                        if (listPartieEnCours.isEmpty()){
                            no_match_T.setTextColor(Color.WHITE)
                            no_match_EC.text = "AUCUN MATCH EN COURS"
                        }
                        else{
                            no_match_EC.text = ""
                            viewModel.setList(listPartieEnCours)
                            viewModel.matchList.observe(this@MainActivity, Observer{
                                recyclerViewEC.adapter = MatchRecyclerViewAdaptater(it){
                                        selectedItem: PartieItem -> listItemClicked(selectedItem)
                                }
                            })
                        }
                    }
                    else{
                        no_match_T.setTextColor(Color.WHITE)
                        no_match_EC.text = "AUCUN MATCH EN COURS"
                    }
                }

                override fun onFailure(call: Call<List<PartieItem>?>, t: Throwable) {
                    no_match_T.setTextColor(Color.RED)
                    no_match_EC.text = "ECHEC DE RECUPERATION DES DONNEES"
                    Log.d("Data Not Found PARTIE EN COURS LAND", "fail to get data, erreur" + t)
                }
            })
            PartieTerminees.enqueue(object : Callback<List<PartieItem>?> {
                override fun onResponse(
                    call: Call<List<PartieItem>?>,
                    response: Response<List<PartieItem>?>
                ) {
                    val reponse = response.body()
                    if(reponse != null){
                        listPartieTermine = reponse
                        if (listPartieTermine.isEmpty()){
                            no_match_T.setTextColor(Color.WHITE)
                            no_match_T.text = "AUCUN MATCH TERMINE"
                        }
                        else{
                            no_match_T.text = ""
                            viewModel.setList(listPartieTermine)
                            viewModel.matchList.observe(this@MainActivity, Observer{
                                recyclerViewT.adapter = MatchRecyclerViewAdaptater(it) {
                                        selectedItem: PartieItem -> listItemClicked(selectedItem)
                                }
                            })
                        }
                    }
                    else {
                        no_match_T.setTextColor(Color.WHITE)
                        no_match_T.text = "AUCUN MATCH TERMINE"
                    }
                }

                override fun onFailure(call: Call<List<PartieItem>?>, t: Throwable) {
                    no_match_T.setTextColor(Color.WHITE)
                    no_match_T.text = "ECHEC DE RECUPERATION DES DONNEES"
                    Log.d("Data Not Found", "fail to get data, erreur" + t)
                }
            })

            binding.ButtonRefreshMainA.setOnClickListener{
                refresh_Land()
            }
        }

        val dateText = binding.DateMainA
        dateText.setText("L'APPLI LA PLUS CLAQUEE DU TENNIS")

    }

    fun refresh_Land(){
        val retrofitBuilder = Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create())
            .baseUrl(URL)
            .build()
            .create(InterfaceAPI::class.java)

        val PartieAVenir = retrofitBuilder.getPartieAVenir()
        val PartieEnCours = retrofitBuilder.getPartieEnCours()
        val PartieTerminees = retrofitBuilder.getPartieTerminees()

        var listPartieAVenir = listOf<PartieItem>()
        var listPartieEnCours = listOf<PartieItem>()
        var listPartieTermine = listOf<PartieItem>()

        var view:View = binding.root
        viewModel = ViewModelProvider(this).get(ListMatch::class.java)

        val recyclerViewAS = view.findViewById<RecyclerView>(R.id.MatchRecyclerViewASUIVRE)
        val recyclerViewEC = view.findViewById<RecyclerView>(R.id.MatchRecyclerViewENCOURS)
        val recyclerViewT = view.findViewById<RecyclerView>(R.id.MatchRecyclerViewTERMINES)

        recyclerViewAS.layoutManager = LinearLayoutManager(this)
        recyclerViewEC.layoutManager = LinearLayoutManager(this)
        recyclerViewT.layoutManager = LinearLayoutManager(this)

        val no_match_T = view.findViewById<TextView>(R.id.no_match_T)
        val no_match_EC = view.findViewById<TextView>(R.id.no_match_EC)
        val no_match_AV = view.findViewById<TextView>(R.id.no_match_AV)

        PartieAVenir.enqueue(object : Callback<List<PartieItem>?> {
            override fun onResponse(
                call: Call<List<PartieItem>?>,
                response: Response<List<PartieItem>?>
            ) {
                val reponse = response.body()
                if(reponse != null){
                    listPartieAVenir = reponse
                    if (listPartieAVenir.isEmpty()){
                        no_match_AV.text = "AUCUN MATCH A VENIR"
                    }
                    else{
                        no_match_AV.text = ""
                        viewModel.setList(listPartieAVenir)
                        viewModel.matchList.observe(this@MainActivity, Observer{
                            recyclerViewAS.adapter = MatchRecyclerViewAdaptater(it){
                                    selectedItem: PartieItem -> listItemClicked(selectedItem)
                            }
                        })
                    }
                }
                else{
                    no_match_T.setTextColor(Color.WHITE)
                    no_match_AV.text = "AUCUN MATCH A VENIR"
                }
            }

            override fun onFailure(call: Call<List<PartieItem>?>, t: Throwable) {
                no_match_T.setTextColor(Color.RED)
                no_match_AV.text = "ECHEC DE RECUPERATION DES DONNEES"
                Log.d("Data Not Found", "fail to get data, erreur" + t)
            }
        })
        PartieEnCours.enqueue(object : Callback<List<PartieItem>?> {
            override fun onResponse(
                call: Call<List<PartieItem>?>,
                response: Response<List<PartieItem>?>
            ) {
                val reponse = response.body()
                if(reponse != null){
                    listPartieEnCours = reponse
                    if (listPartieEnCours.isEmpty()){
                        no_match_T.setTextColor(Color.WHITE)
                        no_match_EC.text = "AUCUN MATCH EN COURS"
                    }
                    else{
                        no_match_EC.text = ""
                        viewModel.setList(listPartieEnCours)
                        viewModel.matchList.observe(this@MainActivity, Observer{
                            recyclerViewEC.adapter = MatchRecyclerViewAdaptater(it){
                                    selectedItem: PartieItem -> listItemClicked(selectedItem)
                            }
                        })
                    }
                }
                else{
                    no_match_T.setTextColor(Color.WHITE)
                    no_match_EC.text = "AUCUN MATCH EN COURS"
                }
            }

            override fun onFailure(call: Call<List<PartieItem>?>, t: Throwable) {
                no_match_T.setTextColor(Color.RED)
                no_match_EC.text = "ECHEC DE RECUPERATION DES DONNEES"
                Log.d("Data Not Found", "fail to get data, erreur" + t)
            }
        })
        PartieTerminees.enqueue(object : Callback<List<PartieItem>?> {
            override fun onResponse(
                call: Call<List<PartieItem>?>,
                response: Response<List<PartieItem>?>
            ) {
                val reponse = response.body()
                if(reponse != null){
                    listPartieTermine = reponse
                    if (listPartieTermine.isEmpty()){
                        no_match_T.setTextColor(Color.WHITE)
                        no_match_T.text = "AUCUN MATCH TERMINE"
                    }
                    else{
                        no_match_T.text = ""
                        viewModel.setList(listPartieTermine)
                        viewModel.matchList.observe(this@MainActivity, Observer{
                            recyclerViewT.adapter = MatchRecyclerViewAdaptater(it) {
                                    selectedItem: PartieItem -> listItemClicked(selectedItem)
                            }
                        })
                    }
                }
                else {
                    no_match_T.setTextColor(Color.WHITE)
                    no_match_T.text = "AUCUN MATCH TERMINE"
                }
            }

            override fun onFailure(call: Call<List<PartieItem>?>, t: Throwable) {
                no_match_T.setTextColor(Color.WHITE)
                no_match_T.text = "ECHEC DE RECUPERATION DES DONNEES"
                Log.d("Data Not Found", "fail to get data, erreur" + t)
            }
        })
    }

    private fun replacerFragment(fragment : Fragment){
        val fragmentManager = supportFragmentManager
        val fragmentTransaction = fragmentManager.beginTransaction()
        fragmentTransaction.replace(R.id.fragmentContainer, fragment)
        fragmentTransaction.commit()
    }

   fun listItemClicked(partieItem: PartieItem) {
       val intent = Intent(this, DetailMatchActivity::class.java);
       intent.putExtra("partie", partieItem)
       this.startActivity(intent);
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        super.onRestoreInstanceState(savedInstanceState)
        //onConfigurationChanged()
    }

}
