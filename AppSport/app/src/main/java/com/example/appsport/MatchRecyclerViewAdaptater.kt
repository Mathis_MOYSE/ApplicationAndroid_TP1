package com.example.appsport

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ExpandableListView.OnChildClickListener
import androidx.recyclerview.widget.RecyclerView

class MatchRecyclerViewAdaptater (
    private val matchList:List<PartieItem>,
    private val clickListener: (PartieItem)->Unit
    ):RecyclerView.Adapter<MatchViewHolder>(){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MatchViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val listItem = layoutInflater.inflate(R.layout.list_item, parent, false)
        return MatchViewHolder(listItem)
    }

    override fun onBindViewHolder(holder: MatchViewHolder, position: Int) {
        val match = matchList[position]
        holder.bind(match, clickListener)
    }

    override fun getItemCount(): Int {
        return matchList.size
    }
}