package com.example.appsport

import android.content.Intent
import android.util.Log
import android.view.View
import android.widget.ExpandableListView.OnChildClickListener
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

class MatchViewHolder(val view: View) : RecyclerView.ViewHolder(view) {
    fun bind(match: PartieItem, clickListener: (PartieItem)->Unit) {

        val joueur1_nom = view.findViewById<TextView>(R.id.Joueur1)
        val joueur2_nom = view.findViewById<TextView>(R.id.Joueur2)
        val lieu = view.findViewById<TextView>(R.id.Lieu)
        val duree = view.findViewById<TextView>(R.id.Duree)

        joueur1_nom.text = match.joueur1.nom
        joueur2_nom.text = match.joueur2.nom
        lieu.text = "Tournoi : " + match.tournoi
        duree.text = match.temps_partie.toString() + " mn"

        view.setOnClickListener{
            clickListener(match)
        }
    }
}