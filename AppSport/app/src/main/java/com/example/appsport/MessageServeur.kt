package com.example.appsport

import java.io.Serializable

data class MessageServeur(
    val message : String
) :  Serializable
