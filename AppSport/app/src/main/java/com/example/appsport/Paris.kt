package com.example.appsport

import java.io.Serializable

data class Paris(
    val gain: Double,
    val id_parieur: String,
    val joueur_mise: String,
    val montant_paris: Double
) : Serializable