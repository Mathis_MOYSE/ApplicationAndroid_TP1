package com.example.appsport

import java.io.Serializable

data class PartieItem(
    val id_partie : Int,
    val id_joueur : Int,
    val contestation: List<Int>,
    val heure_debut: String,
    val joueur1: Joueur,
    val joueur2: Joueur,
    val liste_paris: List<Paris>,
    val liste_gagnants: List<Paris>,
    val nombre_coup_dernier_echange: Int,
    val pointage: Pointage,
    val serveur: Int,
    val temps_partie: Int,
    val terrain: String,
    val tournoi: String,
    val vitesse_dernier_service: Int
) : Serializable