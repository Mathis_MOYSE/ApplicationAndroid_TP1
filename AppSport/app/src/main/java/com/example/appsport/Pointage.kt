package com.example.appsport

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class Pointage(
    val echange: List<Int>,

    @SerializedName("final")
    val fini: Boolean,

    val jeu: List<List<Int>>,
    val manches: List<Int>
) : Serializable